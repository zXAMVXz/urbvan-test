/*global google*/
import React, { useState, useEffect } from "react";
import { withGoogleMap, GoogleMap, DirectionsRenderer } from "react-google-maps";

export const Map = ({origin,destination}) => {

  const [direccion, setDireccion] = useState(null)

  useEffect(() => {

    /* DETONAMOS EL SERVICIO PARA OBTENER LAS DIRECCIONES */
    const directionsService = new google.maps.DirectionsService();
    
    directionsService.route(
      {
        origin: origin,
        destination: destination,
        travelMode: google.maps.TravelMode.DRIVING,
        provideRouteAlternatives:true
      },
      (result, status) => {
        
        if (status === google.maps.DirectionsStatus.OK) {
          console.log(result);
            setDireccion(result);
        } else {
          console.error(`error fetching directions ${result}`);
        }
      }
    );

  }, [])


  const GoogleMapExample = withGoogleMap(() => (

    <GoogleMap
      defaultCenter={{ lat:19.4271, lng:-99.1676 }}
      defaultZoom={15}
    >
      <DirectionsRenderer
        directions={direccion}
        routeIndex={3}
      />
    </GoogleMap>
  ));

  return(

    <div>
        <GoogleMapExample
          containerElement={<div style={{ height: '500px', width: '100%' }} />}
          mapElement={<div style={{ height: '100%' }} />}
        />
    </div>

  );

}

