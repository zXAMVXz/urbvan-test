/*global google*/
import React,{useState} from 'react'
import './Home.css';
import { withScriptjs } from "react-google-maps";
import { Map } from './Map';
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';
import { geocodeByAddress, getLatLng } from 'react-google-places-autocomplete';
import generalConstants from "../constants/general";
import loader from "../assets/loader.gif";
import UrbvanLogo from '../assets/UrbvanLogo.png';


export const Home = () => {
   // OBTENIENDO GOOGLE API KEY
   const { googleApiKey } = generalConstants;  
   console.log(googleApiKey, '<----- google API');
   const MapLoader = withScriptjs(Map);
   const [addresOne, setAddressOne] = useState(null);
   const [addresTwo, setAddressTwo] = useState(null);
   const [coorOrigen, setCoorOrigen ] = useState({});
   const [coorDestino, setCoorDestino] = useState({});
   const [time, setTime] = useState('');
   const [distance, setDistance] = useState('');
   const [showMap, setShowMap] = useState(false);
   const [labelDestinos, setLabelDestinos] = useState({originLabel:'', destinoLabel:''});
   

   const getCoordinatesFromAdress = () => {
      
      console.log(addresOne);
      console.log(addresTwo);

      setLabelDestinos({
         originLabel:addresOne.value.description,
         destinoLabel:addresTwo.value.description
      })

      geocodeByAddress(addresOne.label)
         .then(results => getLatLng(results[0]))
         .then(({ lat, lng }) =>{
            setCoorOrigen({ lat, lng })
      }
         
      ).then(
         geocodeByAddress(addresTwo.label)
         .then(results => getLatLng(results[0]))
            .then(({ lat, lng }) => {
               setCoorDestino({ lat, lng })
            }
           
         )
      )
      setShowMap(true);
   }

   const ResultCard = () => {
      const service = new google.maps.DistanceMatrixService();
     
      service.getDistanceMatrix(
         {
           origins: [coorOrigen],
           destinations: [coorDestino],
           travelMode: google.maps.TravelMode.DRIVING,
           avoidHighways: true,
           
         },(result, status) => {
    
            if(status == 'OK' ){
               const {rows} = result;
               const [ elementos ] = rows;
               console.log(elementos);

               const { elements } = elementos;
               const [ disDur ] = elements;
               const {  distance, duration } = disDur;
               
               console.log(distance.text)
               console.log(duration.text)

            
               setTime(duration.text)
               setDistance(distance.text)
            }else{
               alert('ocurrio un error al obtener distancia y tiempo de trayecto')
            }
   
          }
      
      )
      return(
         <div className="card text-white bg-primary mb-3">
            <div className="card-body">
               <h5>Origen</h5><p>{labelDestinos.originLabel}</p>
               <h5>Destino</h5><p>{labelDestinos.destinoLabel}</p>
                <h5>Distancia: </h5> <p>{distance} </p> 
               <h5>Tiempo aproximado de trayecto en coche:</h5> <p>{time} </p> 
            </div>
          </div>
      )
   }
   
   return (
      <div className="container mt-5">
         <img className="UrbvanLogo" src={UrbvanLogo} alt="UrbvanLogo" />
         <hr/>
         {/* columna izquierda */}
         <div className="row">
            <div className="col-4">
               <h4 className="text-center">BUSQUEDA DE RUTA</h4>
               <div className="form-group">
                  <label>¿ De donde sales ?</label>
                  <GooglePlacesAutocomplete
                     apiKey={googleApiKey}
                     selectProps={{
                        addresOne,
                        onChange: setAddressOne,
                     }}
                  />
                  <small id="emailHelp" className="form-text text-muted">Introduce la direccion de busqueda</small>
               </div>
               <div className="form-group">
                  <label >¿ A donde te diriges ?</label>
                  <GooglePlacesAutocomplete
                     apiKey={googleApiKey}
                     selectProps={{
                        addresTwo,
                        onChange: setAddressTwo,
                     }}
                  />
                  <small id="emailHelp" className="form-text text-muted">Introduce la direccion de busqueda</small>
               </div>
               <button type="button" onClick={() => getCoordinatesFromAdress()} style={{marginBottom:10}} className="btn btn-success btn-lg btn-block">Buscar ruta</button>
               <ul className="list-group">
                  
                  {
                     showMap && <ResultCard />
                  }

               </ul>
            </div>
            {/* columna derecha */}
            <div className="col-8">
               {
                  showMap ?
                  <MapLoader
                     googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${googleApiKey}`}
                     loadingElement={<div style={{ height: `100%` }} />}
                     origin={ coorOrigen }
                     destination={ coorDestino }
                  /> : <>   <p className="mapText"> Por favor seleccione una ruta para mostrarla en el mapa</p>  <img src={loader} alt="loader" /> </>
               }    
            </div>
         </div>
    </div>
   )
}


